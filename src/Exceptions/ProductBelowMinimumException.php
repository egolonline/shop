<?php

namespace EGOL\Shop\Exceptions;

/**
* ProductMinimumException Exception
*/
class ProductBelowMinimumException extends \Exception
{
}