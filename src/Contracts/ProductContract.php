<?php 

namespace EGOL\Shop\Contracts;

interface ProductContract
{
    public function id();

    public function name();

    public function price();

    public function quantity();

    public function sum();

    public function removable();

    public function quantityEditable();

    public function countable();

    public function porto();

    public function __toString();
}