<?php

namespace EGOL\Shop;

use EGOL\Shop\Contracts\ProductContract;
use EGOL\Shop\Exceptions\ProductNotRemovableException;

/**
 * Product Repository für den Shop.
 * Verwaltet die Produkte zur Berechnung des Preises.
 * Über die magische Methode __toString kann der gwohnte Warenkorb String erstellt werden.
 */
class Cart
{
    public $products = [];

    public $shipping;

    protected $tax;

    /**
     * Sucht im Warenkorb nach einem Produkt mit der ID.
     * @param  int $id Produkt ID
     * @return Product
     */
    public function product($id)
    {
        $products = array_filter($this->products, function ($item) use ($id) {
            return $item->id() == $id;
        });

        if (!count($products)) {
            return false;
        }

        return array_shift($products);
    }

    /**
     * Fügt ein Produkt dem Warenkorb hinzu.
     * @param Product $product
     * @return bool
     */
    public function add(ProductContract $product)
    {
        foreach ($this->products as $item) {
            if ($product->id() == $item->id()) {
                $item->add($product->quantity());

                return true;
            }
        }

        $this->products[] = $product;
    }

    /**
     * Versandkosten Object hinzufügen
     * @param Shipping $shipping
     */
    public function addShipping(Shipping $shipping) 
    {
        $this->shipping = $shipping;
    }

    /**
     * Entfernt ein Produkt aus dem Warenkorb
     * @param  Product $product 
     */
    public function remove(ProductContract $product)
    {
        if (! $product->removable()) {
            throw new ProductNotRemovableException;
        }

        $this->products = array_filter($this->products, function ($item) use ($product) {
            return $product != $item;
        });
    }

    /**
     * Entfernt ein Produkt über die Produkt ID
     * @param  int $id 
     */
    public function removeById($id)
    {      
        $this->products = array_filter($this->products, function ($item) use ($id) {
            if (! $item->removable() && $item->id() == $id) {
                throw new ProductNotRemovableException;
            }

            return $item->id() != $id;
        });
    }

    /**
     * Gibt alle Produkte zurück.
     * @return array
     */
    public function products()
    {
        $products = collect($this->products)->filter(function($item) {
            return $item->quantity() > 0;
        });

        $sorted = $products->sortBy(function($item) {
            return $item->id();
        });

        return $sorted->values();
    }

    public function discountableProductsCount()
    {
        return collect($this->products)->filter(function ($item) {
            if ($item->discountable()) {
                return true;
            }
        })->count();
    }

    /**
     * Zählt die Anzahl der Produkte
     * @return int Anzahl der Produkte im Warenkorb
     */
    public function count()
    {
        $count = 0;

        foreach ($this->products() as $product) {
            if (! $product->countable()) {
                continue;
            }
            
            $count += $product->quantity();
        }

        return $count;
    }

    public function hasProductWithPorto()
    {
        foreach ($this->products() as $product) {
            if ($product->porto()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Erzeugt Summe aus Preis und Versand
     * 
     * @return  float 
     */
    public function sum()
    {
        $sum = 0;

        foreach ($this->products() as $value) {
            $sum += $value->price() * $value->quantity();
        }

        return $sum;
    }
    

    /**
     * Erzeugt die Summe aller Produkte im Warenkorb in Euro inkl. Mwst. und Versandkosten
     * @return float
     */
    public function total()
    {
        $total = 0;

        $total += $this->sum();

        if ($this->shipping) {
            $total += $this->shipping->getPrice();
        }

        $total += $this->calculateTax();

        return $total;
    }

    /**
     * Wird die Instance über echo ausgegeben oder direkt in PDO benutzt, wird der 
     * gewohnte Warenkorb String erzeugt.
     * @return string
     */
    public function __toString()
    {
        $collect = array();
        foreach ($this->products() as $product) {
            $collect[] = $product->__toString();
        }

        if ($this->shipping) {
            $collect[] = $this->shipping;
        }

        return implode('#', $collect);
    }

    /**
     * Aktualisiert die Session. Sollte immer dann benutzt werden, wenn der Warenkorb oder Produkte
     * im Warenkorb geändert wurden.
     */
    public function update($key = 'cart')
    {
        $_SESSION[$key] = serialize($this);
    }

    /**
     * Leert den Warenkorb
     */
    public function clean($key = 'cart') 
    {
        $this->products = [];
        $this->update($key);
    }

    public function search($id) 
    {
        $product = array_filter($this->products, function ($item) use ($id) {
            return $item->id() == $id;
        });

        return $product;
    }

    public function calculateTax()
    {
        $tax = 0;
        foreach ($this->products() as $product) {
            $tax += $product->getTaxValue();    
        }
        
        return $tax;
    }
}
