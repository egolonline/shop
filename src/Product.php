<?php

namespace EGOL\Shop;

use EGOL\Shop\Contracts\ProductContract;
use EGOL\Shop\Exceptions\ProductBelowMinimumException;
use EGOL\Shop\Exceptions\ProductLimitReachedException;
use EGOL\Shop\Exceptions\LimitIsSmallerThanMinimumException;
use EGOL\Shop\Exceptions\QuantityIsNotMulipleOfStepsException;
use EGOL\Shop\Exceptions\QuantityIsNotInsidefFixedStepsException;

/**
 * Basisklasse für die Produkte.
 * Die Klasse Cart setzt diese Basisklasse vorraus.
 */
abstract class Product implements ProductContract
{
    /**
     * ID des Produkts
     * @var int
     */
    protected $id;

    /**
     * Name des Produkts
     * @var string
     */
    protected $name;


    /**
     * Beschreibung
     * @var string
     */
    protected $description;

    /**
     * Preis des Produkts
     * @var float
     */
    protected $price;

    /**
     * Menge des Produkts
     * @var int
     */
    protected $quantity;

    /**
     * Definiert, ob das Produkt nur zu einer bestimmten Menge in den Warenkorb gelegt werden kann.
     * @var int
     */
    protected $limit = 0;

    /**
     * Definiert, ob das Produkt erst ab einer bestimmten Menge gekauft werden kann.
     * 
     * @var integer
     */
    protected $minimum = 0;

    /**
     * Definiert, in welchen Schritten die Menge des Produktes erhöht werden kann
     * 
     * @var integer
     */
    protected $steps = 0;

    /**
     * Definiert, in welchen definierten Schritten die Menge des Produktes erhöht werden kann
     * 
     * @var array
     */
    protected $fixedSteps = array();

    /**
     * Definiert, ob das Produkt aus dem Warenkorb entfernt werden kann. Zum Beispiel Versandkosten.
     * @var bool
     */
    protected $removable = true;

    /**
     * Definiert, ob die Discount Berechnung auf das Produkt angewendet werden darf.
     * @var boolean
     */
    protected $discountable = true;

    /**
     * Definiert, ob die Anzahl nach der der Erstellung geändert werden kann
     * @var boolean
     */
    protected $quantityEditable = true;

    /**
     * Definiert, ob dieses Produkt gezählt werden kann
     * @var boolean
     */
    protected $countable = true;

    /**
     * Array aus Discount Objekten
     * @var EGOL\Shop\Discount
     */
    protected $discounts;

    /**
     * Wird Porto aufgeschlagen?
     * @var boolean
     */
    protected $porto;

    /**
     * Mwst.
     * @var float
     */
    protected $tax = 0.19;


    /**
     * Erzeugt ein Produkt
     * @param int  $id              ID des Produkts
     * @param string  $name         Name des Produkts
     * @param float  $price         Preis des Produkts
     * @param integer $quantity     Anzahl des Produkts
     * @param integer $limit        
     * @param integer $minimum 
     * @param integer $steps 
     * @param integer $fixedSteps 
     */
    public function __construct($id, $name, $price, $quantity = 1, $limit = 0, $minimum = 0, $steps = 1, $fixedSteps = array()) 
    {
        $this->id = $id;
        $this->name = $name;
        $this->price = $price;
        $this->quantity = $quantity;
        $this->limit = $limit;
        $this->minimum = $minimum;
        $this->steps = $steps;
        $this->fixedSteps = $fixedSteps;
        $this->porto = true;

        $this->isLimitSmallerThanMinimum();     
        $this->isLimitExceeded($this->quantity);
        $this->isBelowMinimum($this->quantity);
        $this->isQuantityMultipleOfSteps($this->quantity);
        $this->isQuantityValueOfFixedSteps($this->quantity);

        $this->discounts = collect();
    }

    /**
     * Fügt dem Produkt x Menge hinzu
     * @param int $quantity
     * @return bool
     */
    public function add($quantity)
    {
        if (! $this->quantityEditable) {
            return false;
        }

        $this->isBelowMinimum($quantity + $this->quantity);
        $this->isLimitExceeded($quantity + $this->quantity);
        $this->isQuantityMultipleOfSteps($quantity + $this->quantity);
        $this->isQuantityValueOfFixedSteps($this->quantity);

        $this->quantity += $quantity;
    }

    /**
     * Setter.
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    public function setDescription($description)
    {
        $this->description = $description;
    }

    public function setPrice($price)
    {
        $this->price = $price;
    }

    public function setQuantity($quantity)
    {
        if (! $this->quantityEditable) {
            return false;
        }

        $this->isLimitExceeded($quantity);
        $this->isBelowMinimum($quantity);
        $this->isQuantityMultipleOfSteps($quantity);
        $this->isQuantityValueOfFixedSteps($quantity);

        $this->quantity = $quantity;
    }

    public function setRemovable($removable = true)
    {
        $this->removable = (bool) $removable;
    }

    public function setDiscountable($discountable)
    {
        $this->discountable = $discountable;
    }

    public function setQuantityEditable($editable = true) 
    {
        $this->quantityEditable = (bool) $editable;
    }

    public function setLimit($limit = 1)
    {
        $this->limit = (int) $limit;
    }

    public function setMinimum($min = 1)
    {
        $this->minimum = (int) $min;
    }

    public function setPorto($porto = true)
    {
        $this->porto = $porto;
    }

    public function removeLimit() 
    {
        $this->limit = 0;
    }

    public function setCountable($countable) 
    {
        $this->countable = (bool) $countable;
    }

    /**
     * Getter.
     */
    public function id()
    {
        return $this->id;
    }

    public function name()
    {
        return $this->name;
    }

    public function description()
    {
        return $this->description;
    }

    public function price()
    {
        if ($this->discounts->count()) {
            return $this->discountedPrice();
        }

        return $this->price;
    }

    public function porto()
    {
        return $this->porto;
    }

    public function quantity()
    {
        return $this->quantity;
    }

    public function sum()
    {
        return $this->quantity * $this->price();
    }

    public function removable() 
    {
        return $this->removable;
    }

    public function discountable()
    {
        return $this->discountable;
    }

    public function quantityEditable() 
    {
        return $this->quantityEditable;
    }

    public function minimum()
    {
        return $this->minimum;
    }

    public function countable() 
    {
        return $this->countable;
    }

    public function discount()
    {
        return $this->discounts;
    }

    public function steps()
    {
        return $this->steps;
    }

    public function fixedSteps()
    {
        return $this->fixedSteps;
    }

    public function addDiscount(Discount $discount)
    {
        if ($this->discountable) {
            $this->discounts->push($discount);
        }
    }

    public function discountedPrice()
    {
        $price = 0;
        $sorted = $this->discounts->sortBy(function($item) {
            return $item->quantity();
        });

        foreach($sorted as $item) {
            if ($item->quantity() <= $this->quantity()) {
                $price = $item->price();
            }
        }

        if (! $price) {
            return $this->price;
        }

        return $price;
    }

    /**
     * Gibt einen gewohnten Warenkorb String aus
     * Anzahl * name * Preis * ID
     * @return string
     */
    public function __toString()
    {
        return $this->quantity.'*'.$this->name.'*'.$this->price().'*'.$this->id;
    }

    /**
     * Prüfung, ob Maximale Anzahl überschritten wurde.
     * 
     * @param  integer  $quantity Neue Anzahl
     * @return boolean           
     */
    private function isLimitExceeded($quantity) {
        if (! $this->limit) {
            return false;
        }

        if ($quantity > $this->limit) {
            throw new ProductLimitReachedException;
        }
    }

    /**
     * Prüfung, ob Produktanzahl unter dem Minimum liegt.
     * 
     * @param  integer  $quantity Neue Anzahl
     * @return boolean
     */
    private function isBelowMinimum($quantity)
    {
        if (! $this->minimum) {
            return false;
        }

        if ($quantity < $this->minimum) {
            throw new ProductBelowMinimumException;
        }

        return false;
    }

    /**
     * Prüft, ob die Begrenzung kleiner als das Minimum ist.
     * @return boolean
     */
    public function isLimitSmallerThanMinimum()
    {
        if ($this->limit && $this->limit < $this->minimum) {
            throw new LimitIsSmallerThanMinimumException;
        }

        return false;
    }

    /**
     * Prüft, ob die Menge ein vielfaches con $steps ist
     */
    public function isQuantityMultipleOfSteps($quantity)
    {
        if (! $this->steps) {
            return;
        }

        if ($quantity % $this->steps) {
            throw new QuantityIsNotMulipleOfStepsException;
        }
    }

    /**
     * Prüft, ob die Menge inherhalb von $fixedSteps ist
     */
    public function isQuantityValueOfFixedSteps($quantity)
    {
        if (! $this->fixedSteps) {
            return;
        }

        foreach ($this->fixedSteps as $step) {
            if ($step == $quantity) {
                return;
            }
        }

        throw new QuantityIsNotInsidefFixedStepsException;
    }

    public function getTaxValue()
    {
        return ($this->price() * $this->getTax()) * $this->quantity();
    }

    public function setTax($tax)
    {
        $this->tax = $tax;
    }

    public function getTax()
    {
        return $this->tax;
    }
}
