<?php

namespace EGOL\Shop;

/**
* Shipping
*/
class Shipping
{
    protected $name;

    protected $price;

    function __construct($name, $price, $id = 330)
    {
        $this->id = $id;
        $this->name = $name;
        $this->price = $price;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @return  string
     */
    public function __toString()
    {
        return '1*' . $this->name . '*' . $this->price . '*' . $this->id;
    }
}