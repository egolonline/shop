<?php

namespace EGOL\Shop;

/**
* Discount Klasse
*/
class Discount
{
    protected $quantity;

    protected $price;

    public function __construct($quantity, $price)
    {
        $this->quantity = (int) $quantity;
        $this->price = $price;
    }

    /**
     * @return mixed
     */
    public function quantity()
    {
        return $this->quantity;
    }

    /**
     * @return mixed
     */
    public function price()
    {
        return $this->price;
    }
}