<?php

use EGOL\Shop\Cart;
use EGOL\Shop\Discount;
use EGOL\Shop\ExampleProduct;
use PHPUnit\Framework\TestCase;

class DiscountTest extends TestCase
{
    public function setUp() : void
    {
        $this->cart = new Cart;
        $this->product = new ExampleProduct(1, "Gutschein", 10, 50);
        $this->discount1 = new Discount(10, 8);
        $this->discount2 = new Discount(20, 6);
        $this->discount3 = new Discount(30, 4);
    }

    /**
     * @test
     */
    public function canGetDiscountedPrice()
    {
        $this->product->addDiscount($this->discount1);
        $this->product->addDiscount($this->discount2);
        $this->product->addDiscount($this->discount3);

        $this->assertEquals(4, $this->product->price());
    }

    /**
     * @test
     */
    public function canGetTotal()
    {
        $this->product->addDiscount($this->discount3);
        $this->cart->add($this->product);
        $this->assertEquals(238.0, $this->cart->total());
    }

    /**
     * @test
     */
    public function canNotAddDiscountOnNotDiscountableProducts()
    {
        $this->product->setDiscountable(false);
        $this->product->addDiscount($this->discount1);

        $this->assertEquals(0, count($this->product->discount()));
    }

    /**
     * @test
     */
    public function onlyDiscoutableProductsCalculateDiscount()
    {
        $product1 = new ExampleProduct(1, 'Produkt 1', 10, 2);
        $product2 = new ExampleProduct(2, 'Produkt 2', 3, 2);

        $product2->setDiscountable(false);

        $discount = new Discount(2, 2);
        $product1->addDiscount($discount);
        $product2->addDiscount($discount);

        $cart = new Cart();
        $cart->add($product1);
        $cart->add($product2);

        $this->assertEquals(11.9, $cart->total());
    }
}
