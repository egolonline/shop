<?php

use EGOL\Shop\Cart;
use EGOL\Shop\Shipping;
use EGOL\Shop\ExampleProduct;
use PHPUnit\Framework\TestCase;
use EGOL\Shop\Exceptions\ProductNotRemovableException;

/**
 * Cart Test.
 */
class CartTest extends TestCase
{
    protected $cart;

    public function setUp() : void
    {
        $this->cart = new Cart();
    }

    public function testCanBeCreated()
    {
        $this->assertInstanceOf(Cart::class, $this->cart);
    }

    /**
     * @test
     */
    public function canBeAdded()
    {
        $this->cart->add(new ExampleProduct(1, 'Single', 9.99));
        $this->assertCount(1, $this->cart->products());
    }

    /**
     * @test
     */
    public function productCanBeRemoved()
    {
        $product = new ExampleProduct(1, 'Single', 9.99);
        $product2 = new ExampleProduct(2, 'Single 2', 12.99);

        $this->cart->add($product);
        $this->cart->add($product2);

        $this->assertCount(2, $this->cart->products());

        $this->cart->remove($product);

        $this->assertCount(1, $this->cart->products());
    }

    public function productCanBeRemovedById()
    {
        $product = new ExampleProduct(1, 'Single', 9.99);
        $product2 = new ExampleProduct(2, 'Single 2', 12.99);

        $this->cart->add($product);
        $this->cart->add($product2);
        $this->cart->removeById(1);
        $this->cart->removeById(2);

        $this->assertEquals(0, $this->cart->count());
    }

    /**
     * @test
     */
    public function productCanBeRetrievedById()
    {
        $this->cart->add(new ExampleProduct(3, 'Single', 10));
        $product = $this->cart->product(3);

        $this->assertInstanceOf(ExampleProduct::class, $product);
        $this->assertEquals(3, $product->id());
    }

    /**
     * @test
     */
    public function canBeCleared() 
    {
        $this->cart->add(new ExampleProduct(3, 'Single', 10));

        $this->cart->clean();

        $this->assertEquals(0, $this->cart->count());
    }

    /**
     * @test
     */
    public function canBeCounted()
    {
        $this->cart->add(new ExampleProduct(1, 'Single', 10, 2));

        $this->assertEquals(2, $this->cart->count());
    }

    /**
     * @test
     */
    public function noDoublicatsCanBeAdded()
    {
        $this->cart->add(new ExampleProduct(1, 'Single', 10, 1));
        $this->cart->add(new ExampleProduct(1, 'Single', 10, 1));
        $this->cart->add(new ExampleProduct(1, 'Single', 10, 2));

        $this->assertEquals(4, $this->cart->count());
    }

    /**
     * @test
     */
    public function canGetTotal()
    {
        $product = new ExampleProduct(1, 'Single', 9.99);
        $product2 = new ExampleProduct(2, 'Single 2', 12.99, 2);

        $this->cart->add($product);
        $this->cart->add($product2);

        $this->assertEquals(42.8043, $this->cart->total());
    }

    /**
     * @test
     */
    public function canBeEchoedAsString()
    {
        $product = new ExampleProduct(1, 'Single', 9.99);
        $product2 = new ExampleProduct(2, 'Single 2', 12.99);

        $this->cart->add($product);
        $this->cart->add($product2);

        $this->expectOutputString('1*Single*9.99*1#1*Single 2*12.99*2');
        echo $this->cart;
    }

    /**
     * @test
     */
    public function productCanNotBeRemovedFromCart() 
    {
        $product = new ExampleProduct(1, 'Singele', 9);
        $product->setRemovable(false);

        $this->cart->add($product);
        $this->expectException(EGOL\Shop\Exceptions\ProductNotRemovableException::class);
        $this->cart->remove($product);
    }

    /**
     * @test
     */
    public function productCanNotBeRemovedByIdFromCart() 
    {
        $product = new ExampleProduct(1, 'Singele', 9);
        $product2 = new ExampleProduct(2, 'Maxi', 10);
        $product->setRemovable(false);

        $this->cart->add($product);
        $this->cart->add($product2);
        $this->expectException(EGOL\Shop\Exceptions\ProductNotRemovableException::class);
        $this->cart->removeById(1);
    }

    /**
     * @test
     */
    public function canSearchProductById() 
    {
        $product = new ExampleProduct(1, "Single", 10);
        $product2 = new ExampleProduct(2, "Multi", 10);
        $this->cart->add($product);
        $this->cart->add($product2);
        $this->assertEquals(1, count($this->cart->search(1)));
    }

    /**
     * @test
     */
    public function notCountableProductsWillBeIgnored() 
    {
        $product = new ExampleProduct(1, "Single", 10);
        $product2 = new ExampleProduct(2, "Multi", 10);
        $product2->setCountable(false);

        $this->cart->add($product);
        $this->cart->add($product2);

        $this->assertEquals(1, $this->cart->count());
    }

    /**
     * @test
     */
    public function itemsWithZeroQuantityAreIgnored() {
        $product = new ExampleProduct(1, "Single", 10, 3);
        $product2 = new ExampleProduct(2, "Multi", 10, 4);

        $product->setQuantity(0);
        $this->cart->add($product);
        $this->cart->add($product2);

        $this->assertEquals(1, count($this->cart->products()));
    }

    /**
     * @test
     */
    public function canAddShippingToTotalMethod() 
    {
        $product = new ExampleProduct(1, "Single", 3, 1);
        $shipping = new Shipping('Porto und Versand', 5);

        $this->cart->add($product);
        $this->cart->addShipping($shipping);

        $this->assertEquals(8.57, $this->cart->total());
    }

    /**
     * @test
     */
    public function canCalculateTax()
    {
        $product = new ExampleProduct(1, 'Single', 3, 1);
        $this->cart->add($product);
        $this->assertEquals($this->cart->calculateTax(), 0.57);   
    }

    /**
     * @test
     */
    public function canAddTaxToSum()
    {
        $product = new ExampleProduct(1, 'Single', 3, 1);
        $this->cart->add($product);
        $this->assertEquals($this->cart->total(), 3.57);   
    }

    /**
     * @test
     */
    public function canReturnIfCartContainsProductWithPorto()
    {
        $product1 = new ExampleProduct(1, 'Single', 3, 1);
        $product2 = new ExampleProduct(1, 'Single', 3, 1);
        $product2->setPorto(false);

        $this->cart->add($product1);
        $this->cart->add($product2);

        $this->assertEquals($this->cart->hasProductWithPorto(), true);
    }

    /**
     * @test
     */
    public function canReturnProductSumWithShipping()
    {
        $product1 = new ExampleProduct(1, 'Single', 3, 1);
        $product2 = new ExampleProduct(2, 'Double', 6, 1);

        $this->cart->add($product1);
        $this->cart->add($product2);

        $shipping = new Shipping('Versand', 12);

        $this->cart->addShipping($shipping);
        $this->assertEquals($this->cart, '1*Single*3*1#1*Double*6*2#1*Versand*12*330');
    }
}
