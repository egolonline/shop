<?php

use EGOL\Shop\Product;
use EGOL\Shop\Discount;
use EGOL\Shop\ExampleProduct;
use PHPUnit\Framework\TestCase;
/**
 *  Product Test.
 */
class ProductTest extends TestCase
{
    protected $product;

    public function setUp() : void
    {
        $this->product = new ExampleProduct(1, 'Movie', 10);
    }

    /**
     * @test
     */
    public function canBeCreated()
    {
        $this->assertInstanceOf(ExampleProduct::class, $this->product);
    }

    /**
     * @test
     */
    public function canGetName()
    {
        $this->assertEquals('Movie', $this->product->name());
    }

    /**
     * @test
     */
    public function canGetPrice()
    {
        $this->assertEquals(10, $this->product->price());
    }

    /**
     * @test
     */
    public function canGetQuantity()
    {
        $this->assertEquals(1, $this->product->quantity());
    }

    /**
     * @test
     */
    public function canAddQuantity()
    {
        $this->product->add(1);
        $this->assertEquals(2, $this->product->quantity());
    }

    /**
     * @test
     */
    public function canSetQuantity()
    {
        $this->product->setQuantity(3);
        $this->assertEquals(3, $this->product->quantity());
    }

    /**
     * @test
     */
    public function canGetSum()
    {
        $this->product->add(1);
        $this->assertEquals(20, $this->product->sum());
    }

    /**
     * @test
     */
    public function canBeEchoedToString()
    {
        $this->expectOutputString('1*Movie*10*1');
        echo $this->product;
    }

    /**
     * @test
     */
    public function canNotExceedLimitBySetQuanitity()
    {
        $this->product->setLimit(1);
        $this->expectException(EGOL\Shop\Exceptions\ProductLimitReachedException::class);
        $this->product->setQuantity(2);
    }

    /**
     * @test
     */
    public function canNotBeExceedLimitByAdd() 
    {
        $this->product->setLimit(1);
        $this->expectException(EGOL\Shop\Exceptions\ProductLimitReachedException::class);
        $this->product->add(1);
    }

    /**
     * @test
     */
    public function canThrowProductBelowMinimumException()
    {
        $this->product->setMinimum(10);
        $this->expectException(EGOL\Shop\Exceptions\ProductBelowMinimumException::class);
        $this->product->add(8);
    }

    /**
     * @test
     */
    public function canThrowProductBelowMinimumExceptionWhenObjectIsCreated()
    {
        $this->expectException(EGOL\Shop\Exceptions\ProductBelowMinimumException::class);
        $product = new ExampleProduct(1, "Product", 10, 4, 0, 5);
    }

    /**
     * @test
     */
    public function canGetDiscountedSum()
    {
        $product = new ExampleProduct(1, "Product", 10, 1);
        $product->addDiscount(new Discount(10, 4));
        $product->addDiscount(new Discount(20, 3));
        $product->addDiscount(new Discount(30, 2));
        $product->addDiscount(new Discount(40, 1));
        
        $this->assertEquals($product->sum(), 10);

        $product->setQuantity(15);
        $this->assertEquals($product->sum(), 60);

        $product->setQuantity(23);
        $this->assertEquals($product->sum(), 69);

        $product->setQuantity(66);
        $this->assertEquals($product->sum(), 66);
    }

    /**
     * @test
     */
    public function canSetQuantityAsSteps()
    {
        $product = new ExampleProduct(1, "Product", 10, 500, 2500, 500, 500);
        $this->assertEquals($product->steps(), 500);
    }

    /**
     * @test
     */
    public function canThrowQuantityIsNotMulitpleOfStepsException()
    {
        $this->expectException(EGOL\Shop\Exceptions\QuantityIsNotMulipleOfStepsException::class);
        $product = new ExampleProduct(1, "Product", 10, 501, 2500, 500, 500);   
    }

    /**
     * @test
     */
    public function canSetFixedStepsQuantity()
    {
        $product = new ExampleProduct(1, "Product", 10, 500, 2500, 500, 0, array(500, 1000, 2500));
        $this->assertEquals($product->fixedSteps(), array(500, 1000, 2500));
    }

    /**
     * @test
     */
    public function canThrowQuantityIsNotInsodeOfFixedStepsException()
    {
        $this->expectException(EGOL\Shop\Exceptions\QuantityIsNotInsidefFixedStepsException::class);
        $product = new ExampleProduct(1, "Product", 10, 501, 2500, 500, 0, array(500, 1000, 2500));
    }

    /**
     * @test
     */
    public function canSetPortoStatus()
    {
        $product = new ExampleProduct(1, "Product", 10, 1);
        $product->setPorto(false);
        $this->assertEquals($product->porto(), false);
    }

    /**
     * @test
     */
    public function canReturnPortoStatus()
    {
        $product = new ExampleProduct(1, "Product", 1, 10);
        $this->assertEquals($product->porto(), true);   
    }
}
